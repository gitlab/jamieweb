<?php date_default_timezone_set("Europe/London");
include "response-headers.php"; content_security_policy();
//The stats ideally should all be stored in a JSON object, but this is quite an old pipeline so it would require a significant change and testing. Validation is performed before and after they reach the server to ensure security and order, so this setup is reliable (and marginally inefficient) for the time being.
function filter_stat($stat) {
    return(substr(filter_var($stat, FILTER_SANITIZE_FULL_SPECIAL_CHARS, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH), 0, 21));
}

function checkLow($stat, $threshold, $name) {
    if(str_replace(",", "", $stat) > $threshold) {
        return($stat);
    } else {
        return($stat . "<!--" . $name . " seems low. An alert has been sent to Jamie.-->");
    }
}

//foreach(array("master", "node1", "node2", "node3", "node4") as $stats) {
//    $$stats = array_map("filter_stat", file("computing-stats/stats/" . $stats . ".txt", FILE_USE_INCLUDE_PATH | FILE_IGNORE_NEW_LINES));
//    if($$stats[0] > 99.9) {
//        $$stats[0] = "100%";
//    }
//}

$now = new DateTime('2022-01-21');
$masterStatus = "limegreen";
$statusMsg = "As of 21st January 2022, after over 6 years of continual 24/7 operation, this project has been decommissioned. The statistics on this page remain an accurate final snapshot from the last time the device phoned home.";
//file_get_contents("computing-stats/status-message.txt", FILE_USE_INCLUDE_PATH, NULL, 0, 256);
$e_lastUpdated = "override";
if(!($statusMsg)) {
    try {
        $lastUpdated = date_create_from_format("Y-m-d g:i:sa", $master[8]);
        if($lastUpdated > new DateTime('21 minutes ago')) {
            $statusMsg = "<span class=\"color-limegreen\">All systems operational.</span>";
        } else {
            $masterStatus = "orange";
            $statusMsg = "<span class=\"color-orange\">System appears to have lost connection. An alert has been sent to Jamie.</span>";
        }
    } catch(Exception $e_lastUpdated) {
        $statusMsg = "<span class=\"color-orange\">Error checking system status. An alert has been sent to Jamie.</span>";
    }
} ?>
<!DOCTYPE html>
<html lang="en">

<!--Copyright Jamie Scaife-->
<!--Legal Information at https://www.jamieweb.net/contact-->

<head>
    <title>Raspberry Pi Cluster Live Stats</title>
    <meta name="description" content="Live system statistics from my Raspberry Pi cluster, which has been running 24/7 in my garage since October 2015.">
    <?php include "head.php" ?>
    <link href="https://www.jamieweb.net/projects/computing-stats/" rel="canonical">
</head>

<body>

<?php include "navbar.php" ?>

<div class="body">
    <h1 class="two-mar-bottom">Raspberry Pi Cluster Live Stats</h1>
    <table width="100%">
        <tr>
            <!-- Note to self: Fix the ridiculous number of classes here. -->
            <td bgcolor="<?php echo $masterStatus; ?>" width="20%"><div class="centertext"><h2>Master</h2></div></td>
            <td bgcolor="crimson" width="20%"><div class="centertext"><h2>Node 1</h2></div></td>
            <td bgcolor="crimson" width="20%"><div class="centertext"><h2>Node 2</h2></div></td>
            <td bgcolor="crimson" width="20%"><div class="centertext"><h2>Node 3</h2></div></td>
            <td bgcolor="crimson" width="20%"><div class="centertext"><h2>Node 4</h2></div></td>
        </tr>
        <tr>
            <td width="20%"><p class="info">CPU: <b>100%</b></p>
            <p class="info">Memory: <b>595 MB</b></p>
            <p class="info">Disk: <b>1540 MB</b></p>
            <p class="info">Temp: <b>23.8'C</b></td>

            <td width="20%"><p class="info">CPU: <b>90.9%</b></p>
            <p class="info">Memory: <b>373 MB</b></p>
            <p class="info">Disk: <b>4999 MB</b></p>
            <p class="info">Temp: <b>30.4'C</b></td>

            <td width="20%"><p class="info">CPU: <b>89.6%</b></p>
            <p class="info">Memory: <b>211 MB</b></p>
            <p class="info">Disk: <b>4998 MB</b></p>
            <p class="info">Temp: <b>30.4'C</b></td>

            <td width="20%"><p class="info">CPU: <b>99.6%</b></p>
            <p class="info">Memory: <b>221 MB</b></p>
            <p class="info">Disk: <b>5001 MB</b></p>
            <p class="info">Temp: <b>31.5'C</b></td>

            <td width="20%"><p class="info">CPU: <b>99.3%</b></p>
            <p class="info">Memory: <b>401 MB</b></p>
            <p class="info">Disk: <b>4998 MB</b></p>
            <p class="info">Temp: <b>31.5'C</b></td>
        </tr>
    </table><br>
    <p class="info">Stats Update Every 10 Minutes. Last Updated: <b><?php if(isset($e_lastUpdated)) {
    echo "<span class=\"color-orange\">Unknown</span>";
} else {
    echo $lastUpdated->format('g:i:sa') . " " . ($lastUpdated->format('I') ? "GMT+1" : "GMT");
} ?></b></p><br>
    <p class="info">System Status Message: <b><?php echo $statusMsg; ?></b></p><br>

    <h1 class="info">Current Project: <span class="currentproject">Einstein@Home</span></h1>
    <p>The cluster is currently running <a href="https://einsteinathome.org/" target="_blank" rel="noopener">Einstein@Home</a>, which is a distributed computing project that searches for gravitational waves using data from the LIGO gravitational wave detector.</p>

    <div class="computing-stats">
        <div>
            <h1 class="info">Einstein@Home Stats</h1>
            <p class="info">Total Earned Credits: <b>1,541,971.03</b></p>
            <p class="info">Recent Average Credit: <b>1,001.72</b></p>
            <p class="info">Total Running Time: <b><?php echo $now->diff(new DateTime('2015-10-04'))->format("%a"); ?> days</b></p>
        </div>
    </div>
</div>

<?php include "footer.php" ?>

</body>

</html>
